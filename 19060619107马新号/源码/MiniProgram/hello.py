from flask import Flask, request
from wx_login_or_register import get_access_code, get_wx_user_info, login_or_register
from model import db

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://root:realooc@localhost:3306/test?charset=utf8mb4'
app.config['FLASK_ENV'] = 'development'
# 注册数据库连接
db.app = app
db.init_app(app)


@app.route("/testWXLoginOrRegister", methods=["GET"])
def test_wx_login_or_register():
    """
    测试微信登陆注册
    :return:
    """
    # 前端获取到的临时授权码
    code = request.args.get("code")
    # 标识web端还是app端登陆或注册
    flag = request.args.get("flag")

    # 参数错误
    if code is None or flag is None:
        return "参数错误"

    # 获取微信用户授权码
    access_code = get_access_code(code=code, flag=flag)
    if access_code is None:
        return "获取微信授权失败"

    # 获取微信用户信息
    wx_user_info = get_wx_user_info(access_data=access_code)
    if wx_user_info is None:
        return "获取微信授权失败"

    # 验证微信用户信息本平台是否有，
    data = login_or_register(wx_user_info=wx_user_info)
    if data is None:
        return "登陆失败"
    return data


if __name__ == "__main__":
    app.run()
