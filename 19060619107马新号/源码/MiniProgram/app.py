from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
db = SQLAlchemy(app)
app.config['SQLALCHEMY_DATABASE_URL'] = 'mysql://root:123@localhost:3306/test'


@app.route('/')
def hello():
    print(request.path)
    print(request.full_path)
    return request.args.__str__()


@app.route('/register', methods=["POST"])
def register():
    print(request.headers)
    print(request.stream.read())


if __name__ == '__main__':
    app.run(port=5000, debug=True)
